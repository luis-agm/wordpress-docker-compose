## Wordpress with docker-compose

Simple docker-compose file to spin up a wordpress site. Uses traefik as reverse proxy to distribute traffic.
